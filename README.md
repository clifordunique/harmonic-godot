harmonic-godot is a library of reusable Godot scripts and resources for use in my games.  I use it as starter/shared code in most projects.

This is not a mature library by any means and should be used at your own peril.