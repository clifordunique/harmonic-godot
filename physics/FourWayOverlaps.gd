extends Node2D

class_name FourWayOverlaps

export var _width: float = 1.0
export var _distance: float = 100.0

var _leftArea: Area2D = Area2D.new()
var _downArea: Area2D = Area2D.new()
var _upArea: Area2D = Area2D.new()
var _rightArea: Area2D = Area2D.new()
var _areas: Array = [_leftArea, _downArea, _upArea, _rightArea]


func _ready() -> void:
	var rect = RectangleShape2D.new()
	rect.extents = Vector2(_distance * 0.5, _width * 0.5)
	for area in _areas:
		add_child(area)
		var collisionShape: CollisionShape2D = CollisionShape2D.new()
		collisionShape.shape = rect
		area.add_child(collisionShape)

	_leftArea.position = Vector2(-_distance * 0.5, 0)
	_rightArea.position = Vector2(_distance * 0.5, 0)
	_upArea.position = Vector2(0, -_distance * 0.5)
	_downArea.position = Vector2(0, _distance * 0.5)
	_upArea.rotation_degrees = 90
	_downArea.rotation_degrees = 90


func GetOverlaps(facing: int) -> Array:
	var result: Array = _areas[facing].get_overlapping_areas()
	result.append_array(_areas[facing].get_overlapping_bodies())
	for area in _areas:
		result.erase(area)
	result.sort_custom(self, "_distanceSort")
	return result


func _distanceSort(a: Node2D, b: Node2D) -> bool:
	return (
		a.global_position.distance_squared_to(global_position)
		< b.global_position.distance_squared_to(global_position)
	)
