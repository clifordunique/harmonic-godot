class_name SiteLock

const kPrefixes: Array = [
	"http://www.",
	"https://www.",
	"http://",
	"https://",
]

static func HasViolation(allowedReferrers: Array, allowedUrls: Array) -> bool:
	if OS.get_name() != "HTML5":
		return false

	var referrer: String = JavaScript.eval("document.referrer", true)
	var absoluteUrl: String = JavaScript.eval("window.location.href", true)

	return (
		! _containsMatch(referrer, allowedReferrers)
		&& ! _containsMatch(absoluteUrl, allowedUrls)
	)

static func _containsMatch(url, whitelist: Array) -> bool:
	if url == null || url == "":
		return false

	for prefix in kPrefixes:
		url = url.trim_prefix(prefix)

	for regexString in whitelist:
		var regex = RegEx.new()
		regex.compile(regexString)
		if regex.search(url) != null:
			return true

	return false
