# Usage:
# yield(ScreenTransitioner.TransitionOut(1, ScreenTransitioner.Type.DIAMONDS), "completed")
# # Change scenes here
# yield(ScreenTransitioner.TransitionIn(1, ScreenTransitioner.Type.DIAMONDS), "completed")
# # Transition done

extends CanvasLayer

enum {
	DIAMONDS = 0,
	FADE = 1,
}

const SHADERS: Dictionary = {
	DIAMONDS: preload("transition-diamonds.shader"),
	FADE: preload("transition-fade.shader"),
}


func SetParams(params: Dictionary = {}) -> void:
	for param in params:
		$ColorRect.material.set_shader_param(param, params[param])


func TransitionOut(duration: float, type: int, color: Color = Color.black) -> void:
	$ColorRect.color = color
	$ColorRect.material.shader = SHADERS[type]
	$ColorRect.material.set_shader_param("reverse", 0)
	$ColorRect.material.set_shader_param("progress", 0)
	$ColorRect.visible = true
	$Tween.interpolate_method(
		self, "_onTweenUpdate", 0, 1, duration, Tween.TRANS_LINEAR, Tween.EASE_IN
	)
	$Tween.start()
	yield($Tween, "tween_completed")


func TransitionIn(duration: float, type: int, color: Color = Color.black) -> void:
	$ColorRect.color = color
	$ColorRect.material.shader = SHADERS[type]
	$ColorRect.material.set_shader_param("reverse", 1)
	$ColorRect.material.set_shader_param("progress", 0)
	$Tween.interpolate_method(
		self, "_onTweenUpdate", 0, 1, duration, Tween.TRANS_LINEAR, Tween.EASE_IN
	)
	$Tween.start()
	yield($Tween, "tween_completed")
	$ColorRect.visible = false


func InstantOut(color: Color = Color.black) -> void:
	$ColorRect.color = color
	$ColorRect.material.set_shader_param("reverse", 0)
	$ColorRect.material.set_shader_param("progress", 1)
	$ColorRect.visible = true


func InstantIn() -> void:
	$ColorRect.visible = false


func _ready() -> void:
	self.pause_mode = Node.PAUSE_MODE_PROCESS
	$ColorRect.visible = false
	$ColorRect.material.set_shader_param("progress", 0)
	$ColorRect.material.set_shader_param("reverse", 0)


func _onTweenUpdate(progress: float) -> void:
	$ColorRect.material.set_shader_param("progress", progress)
