shader_type canvas_item;

uniform float progress = 0.5;
uniform float reverse = 0f;
uniform float diamondPixelSize = 10f;
uniform vec2 uvOffset = vec2(1f, 1f);

bool transition(vec2 fragCoord, vec2 uv) {
	float testValue = progress * (1f + uvOffset.x + uvOffset.y) - dot(uv, uvOffset);
	testValue = testValue + reverse * (1f - 2f * testValue);
	bool nextImage = abs(fract(fragCoord.x / diamondPixelSize) - 0.5) + abs(fract(fragCoord.y / diamondPixelSize) - 0.5) > testValue;
	return nextImage;
}

void fragment() {
	if (transition(FRAGCOORD.xy, UV)) {
		discard;
	}
}
