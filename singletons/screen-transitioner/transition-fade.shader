shader_type canvas_item;

uniform float progress = 0.5f;
uniform float reverse = 1f;

void fragment() {
	COLOR.a = COLOR.a * (progress + reverse * (1f - 2f * progress));
}
