class_name Node2DShaker

static func Shake(target: Node2D, duration: float, amount: float, resetZero: bool = true) -> void:
	var originalPosition: Vector2 = target.position
	if resetZero:
		originalPosition = Vector2.ZERO
	var startTimeMs: int = OS.get_ticks_msec()
	var endTimeMs: int = int(round(startTimeMs + duration * 1000))
	while OS.get_ticks_msec() < endTimeMs:
		if ! is_instance_valid(target):
			return
		if target.get_tree().paused:
			return
		target.position = originalPosition + _randVector2(amount)
		yield(target.get_tree(), "idle_frame")
	if ! is_instance_valid(target):
		return
	target.position = originalPosition

static func _randVector2(length: float) -> Vector2:
	var result: Vector2 = Vector2.RIGHT * length
	return result.rotated(rand_range(0, 2 * PI))
