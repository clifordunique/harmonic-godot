# BlinkingCanvasItem
# Attach to any CanvasItem to make it periodically blink between visible/invisible.`

extends CanvasItem

export var Period: float = 1


func _process(delta: float) -> void:
	visible = fmod(OS.get_ticks_msec() * 0.001, Period) > Period * 0.5
